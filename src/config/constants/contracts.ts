export default {
  cake: {
    56: '0x0180A1Ce9D6cCf7bbaB1E0F2aA22de9a98CF5584',
    97: '0x0180A1Ce9D6cCf7bbaB1E0F2aA22de9a98CF5584',
  },
  masterChef: {
    56: '0x912fc98D4b196C6e064ddECA92317bF9DD4342CF',
    97: '',
  },
  wbnb: {
    56: '0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c',
    97: '',
  },
  lottery: {
    56: '',
    97: '',
  },
  lotteryNFT: {
    56: '',
    97: '',
  },
  mulltiCall: {
    56: '0x505511658d098A40f11209902D1B8684319c4b72',
    97: '0x505511658d098A40f11209902D1B8684319c4b72',
  },
  busd: {
    56: '0xe9e7cea3dedca5984780bafc599bd69add087d56',
    97: '',
  },
}
