import contracts from './contracts'
import { FarmConfig, QuoteToken } from './types'

const farms: FarmConfig[] = [
  {
    pid: 1,
    lpSymbol: 'BANANAZ-BUSD LP',
    lpAddresses: {
      97: '',
      56: '0x7239b2e7a3fc4c468a6c1c2cc2ca89fb3627868a',
    },
    tokenSymbol: 'BANANAZ',
    tokenAddresses: {
      97: '',
      56: '0x0180A1Ce9D6cCf7bbaB1E0F2aA22de9a98CF5584',
    },
    quoteTokenSymbol: QuoteToken.BUSD,
    quoteTokenAdresses: contracts.busd,
  },
  {
    pid: 2,
    lpSymbol: 'BANANAZ-BNB LP',
    lpAddresses: {
      97: '',
      56: '0xf51dccd7b13e5648942d710a6fed2ae6303835d4',
    },
    tokenSymbol: 'BANANAZ',
    tokenAddresses: {
      97: '',
      56: '0x0180A1Ce9D6cCf7bbaB1E0F2aA22de9a98CF5584',
    },
    quoteTokenSymbol: QuoteToken.BNB,
    quoteTokenAdresses: contracts.wbnb,
  },
  {
    pid: 3,
    lpSymbol: 'BNB-BUSD LP',
    lpAddresses: {
      97: '',
      56: '0x1B96B92314C44b159149f7E0303511fB2Fc4774f',
    },
    tokenSymbol: 'BNB',
    tokenAddresses: {
      97: '',
      56: '0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c',
    },
    quoteTokenSymbol: QuoteToken.BUSD,
    quoteTokenAdresses: contracts.busd,
  },
  {
    pid: 4,
    lpSymbol: 'BTCB-BNB LP',
    lpAddresses: {
      97: '',
      56: '0x7561eee90e24f3b348e1087a005f78b4c8453524',
    },
    tokenSymbol: 'BTCB',
    tokenAddresses: {
      97: '',
      56: '0x7130d2A12B9BCbFAe4f2634d864A1Ee1Ce3Ead9c',
    },
    quoteTokenSymbol: QuoteToken.BNB,
    quoteTokenAdresses: contracts.wbnb,
  },
  {
    pid: 5,
    lpSymbol: 'ETH-BNB LP',
    lpAddresses: {
      97: '',
      56: '0x70d8929d04b60af4fb9b58713ebcf18765ade422',
    },
    tokenSymbol: 'ETH',
    tokenAddresses: {
      97: '',
      56: '0x2170Ed0880ac9A755fd29B2688956BD959F933F8',
    },
    quoteTokenSymbol: QuoteToken.BNB,
    quoteTokenAdresses: contracts.wbnb,
  },
  // caves
  {
    pid: 0,
    isTokenOnly: true,
    lpSymbol: 'BANANAZ',
    lpAddresses: {
      97: '',
      56: '0x7239b2e7a3fc4c468a6c1c2cc2ca89fb3627868a', // BANANAZ-BUSD LP
    },
    tokenSymbol: 'BANANAZ',
    tokenAddresses: {
      97: '',
      56: '0x0180A1Ce9D6cCf7bbaB1E0F2aA22de9a98CF5584',
    },
    quoteTokenSymbol: QuoteToken.BUSD,
    quoteTokenAdresses: contracts.busd,
  },
]

export default farms
