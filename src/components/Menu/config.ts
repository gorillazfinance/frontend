import { MenuEntry } from '@gorillaz/uikit'

const config: MenuEntry[] = [
  {
    label: 'Home',
    icon: 'HomeIcon',
    href: '/',
  },
  {
    label: 'Trade',
    icon: 'TradeIcon',
    items: [
      {
        label: 'Exchange',
        href: 'https://exchange.pancakeswap.finance/#/swap?outputCurrency=0x0180A1Ce9D6cCf7bbaB1E0F2aA22de9a98CF5584',
        external: true,
      },
      {
        label: 'Liquidity',
        href: 'https://exchange.pancakeswap.finance/#/add/BNB/0x0180A1Ce9D6cCf7bbaB1E0F2aA22de9a98CF5584',
        external: true,
      },
    ],
  },
  {
    label: 'Farms',
    icon: 'FarmIcon',
    href: '/farms',
  },
  {
    label: 'Caves',
    icon: 'CaveIcon',
    href: '/caves',
  },
  {
    label: 'Info',
    icon: 'InfoIcon',
    items: [
      {
        label: 'PancakeSwap',
        href: 'https://pancakeswap.info/token/0x0180A1Ce9D6cCf7bbaB1E0F2aA22de9a98CF5584',
        external: true,
      },
    ],
  },
  {
    label: 'Blog',
    icon: 'MediumIcon',
    href: 'https://gorillazfinance.medium.com',
    external: true,
  },
]

export default config
