import React from 'react'
import { Card, CardBody, Heading, Text } from '@gorillaz/uikit'
import BigNumber from 'bignumber.js/bignumber'
import styled from 'styled-components'
import { getBalanceNumber } from 'utils/formatBalance'
import { useTotalSupply, useBurnedBalance } from 'hooks/useTokenBalance'
import useI18n from 'hooks/useI18n'
import { getCakeAddress } from 'utils/addressHelpers'
import CardValue from './CardValue'
import { useFarms, usePriceGorillazBusd } from '../../../state/hooks'

const StyledGorillazStats = styled(Card)`
  margin-left: auto;
  margin-right: auto;
`

const Row = styled.div`
  align-items: center;
  display: flex;
  font-size: 14px;
  justify-content: space-between;
  margin-bottom: 8px;
`

const GorillazStats = () => {
  const TranslateString = useI18n()
  const totalSupply = useTotalSupply()
  const burnedBalance = useBurnedBalance(getCakeAddress())
  const farms = useFarms()
  const gorillazPrice = usePriceGorillazBusd()
  const circSupply = totalSupply ? totalSupply.minus(burnedBalance) : new BigNumber(0)
  const gorillazSupply = getBalanceNumber(circSupply)
  const marketCap = gorillazPrice.times(circSupply)

  let bananazPerBlock = 0
  if (farms && farms[0] && farms[0].bananazPerBlock) {
    bananazPerBlock = new BigNumber(farms[0].bananazPerBlock).div(new BigNumber(10).pow(18)).toNumber()
  }

  return (
    <StyledGorillazStats>
      <CardBody>
        <Heading size="xl" mb="24px">
          {TranslateString(534, 'BANANAZ Stats')}
        </Heading>
        <Row>
          <Text fontSize="14px">{TranslateString(536, 'Total BANANAZ Supply')}</Text>
          {gorillazSupply && <CardValue fontSize="14px" value={gorillazSupply} decimals={0} />}
        </Row>
        <Row>
          <Text fontSize="14px">{TranslateString(999, 'Market Cap')}</Text>
          <CardValue fontSize="14px" value={getBalanceNumber(marketCap)} decimals={0} prefix="$" />
        </Row>
        <Row>
          <Text fontSize="14px">{TranslateString(538, 'Total BANANAZ Burned')}</Text>
          <CardValue fontSize="14px" value={getBalanceNumber(burnedBalance)} decimals={0} />
        </Row>
        <Row>
          <Text fontSize="14px">{TranslateString(540, 'New BANANAZ/block')}</Text>
          <Text bold fontSize="14px">
            {bananazPerBlock}
          </Text>
        </Row>
      </CardBody>
    </StyledGorillazStats>
  )
}

export default GorillazStats
