import { useEffect } from 'react'
import { usePriceGorillazBusd } from 'state/hooks'

const useGetDocumentTitlePrice = () => {
  const gorillazPriceUsd = usePriceGorillazBusd()

  const gorillazPriceUsdString = gorillazPriceUsd.eq(0)
    ? ''
    : ` - $${gorillazPriceUsd.toNumber().toLocaleString(undefined, {
        minimumFractionDigits: 3,
        maximumFractionDigits: 3,
      })}`

  useEffect(() => {
    document.title = `GORILLAZ.FINANCE${gorillazPriceUsdString}`
  }, [gorillazPriceUsdString])
}
export default useGetDocumentTitlePrice
